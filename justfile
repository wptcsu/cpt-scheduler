build-server:
    cd ./server; cargo build
    typeshare ./server --lang=typescript --output-file=./frontend/shared-types.ts
    
typeshare:
    typeshare ./server --lang=typescript --output-file=./frontend/shared-types.ts

init-postgres:
    sudo mkdir -p /run/postgresql
    initdb -D .postgres
    echo "CREATE DATABASE cpt_schedule" | postgres --single -D .postgres postgres

run-dev:
    [ ! -d /run/postgresql ] && sudo mkdir -m 777 -p /run/postgresql || true
    mprocs --config ./mprocs.yaml

check:
    cd ./server; cargo clippy