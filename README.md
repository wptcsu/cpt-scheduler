# Sceideal

Irish; /ˈʃcɛdʲəlˠ/

A simple service-type-focused appointment scheduling application.


## The Dev Environment

Setting up postgres using nix-shell:
```
nix-shell --command "just init-postgres"
```

Copy `server/example_config.toml` to `server/dev_config.yaml`, and then update its parameters.

To run to the dev environment:
```
nix-shell --command "just run-dev"
```